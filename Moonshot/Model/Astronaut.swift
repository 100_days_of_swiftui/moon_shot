//
//  Astronaut.swift
//  Moonshot
//
//  Created by Hariharan S on 22/05/24.
//

import Foundation

struct Astronaut: Codable, Identifiable {
    let id: String
    let name: String
    let description: String
}
