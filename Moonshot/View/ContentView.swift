//
//  ContentView.swift
//  Moonshot
//
//  Created by Hariharan S on 21/05/24.
//

import SwiftUI

struct ContentView: View {
    
    // MARK: - Properties

    let missions: [Mission] = Bundle.main.decode("missions.json")
    let astronauts: [String: Astronaut] = Bundle.main.decode("astronauts.json")
    
    // MARK: - State Property

    @State private var showingGrid: Bool = false
    
    var body: some View {
        NavigationStack {
            Group {
                if self.showingGrid {
                    GridLayoutView(
                        missions: self.missions,
                        astronauts: self.astronauts
                    )
                } else {
                    ListLayoutView(
                        missions: self.missions,
                        astronauts: self.astronauts
                    )
                }
            }
            .navigationTitle("Moonshot")
            .toolbar {
                Button(
                    action: {
                        self.showingGrid.toggle()
                    }, label: {
                        Image(
                            systemName: self.showingGrid
                            ? "square.grid.2x2" : "list.bullet"
                        )
                    }
                )
            }
        }
    }
}

#Preview {
    ContentView()
}

extension ShapeStyle where Self == Color {
    static var darkBackground: Color {
        Color(red: 0.1, green: 0.1, blue: 0.2)
    }

    static var lightBackground: Color {
        Color(red: 0.2, green: 0.2, blue: 0.3)
    }
}
