//
//  GridLayoutView.swift
//  Moonshot
//
//  Created by Hariharan S on 24/05/24.
//

import SwiftUI

struct GridLayoutView: View {
    
    // MARK: - Properties

    let missions: [Mission]
    let astronauts: [String: Astronaut]
    let columns = [
        GridItem(
            .adaptive(minimum: 150)
        )
    ]
    
    var body: some View {
        NavigationStack {
            ScrollView {
                LazyVGrid(columns: self.columns) {
                    ForEach(self.missions) { mission in
                        NavigationLink {
                            MissionView(
                                mission: mission,
                                astronauts: self.astronauts
                            )
                        } label: {
                            VStack {
                                Image(mission.image)
                                    .resizable()
                                    .scaledToFit()
                                    .frame(width: 100, height: 100)
                                    .padding()
                                VStack {
                                    Text(mission.displayName)
                                        .font(.headline)
                                        .foregroundStyle(.white)
                                    Text(mission.formattedLaunchDate)
                                        .font(.caption)
                                        .foregroundStyle(.white.opacity(0.5))
                                }
                                .padding(.vertical)
                                .frame(maxWidth: .infinity)
                                .background(.lightBackground)
                                .clipShape(.rect(cornerRadius: 10))
                                .overlay(
                                    RoundedRectangle(cornerRadius: 10)
                                        .stroke(.lightBackground)
                                )
                            }
                        }
                    }
                }
            }
            .padding([.horizontal, .bottom])
            .preferredColorScheme(.dark)
        }
    }
}

#Preview {
    GridLayoutView(
        missions: Bundle.main.decode("missions.json"),
        astronauts: Bundle.main.decode("astronauts.json")
    )
}
