//
//  ListLayoutView.swift
//  Moonshot
//
//  Created by Hariharan S on 24/05/24.
//

import SwiftUI

struct ListLayoutView: View {
    
    // MARK: - Properties

    let missions: [Mission]
    let astronauts: [String: Astronaut]
    
    var body: some View {
        NavigationStack {
            List (self.missions) { mission in
                NavigationLink {
                    MissionView(
                        mission: mission,
                        astronauts: self.astronauts
                    )
                } label: {
                    HStack {
                        Image(mission.image)
                            .resizable()
                            .scaledToFit()
                            .frame(width: 100, height: 100)
                            .padding()
                        VStack {
                            Text(mission.displayName)
                                .font(.headline)
                                .foregroundStyle(.white)
                            Text(mission.formattedLaunchDate)
                                .font(.caption)
                                .foregroundStyle(.white.opacity(0.5))
                        }
                        .padding(.vertical)
                        .frame(maxWidth: .infinity)
                        .background(.lightBackground)
                        .clipShape(.rect(cornerRadius: 10))
                        .overlay(
                            RoundedRectangle(cornerRadius: 10)
                                .stroke(.lightBackground)
                        )
                    }
                }
            }
            .listStyle(.plain)
            .preferredColorScheme(.dark)
        }
    }
}

#Preview {
    ListLayoutView(
        missions: Bundle.main.decode("missions.json"),
        astronauts: Bundle.main.decode("astronauts.json")
    )
}
